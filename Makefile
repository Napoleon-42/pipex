# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lnelson <lnelson@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2021/10/19 18:08:12 by lnelson           #+#    #+#              #
#    Updated: 2021/10/30 18:30:04 by lnelson          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME			= pipex

SRCS_DIR        = srcs/

OBJS_DIR        = objs/

INCLS_DIR       = srcs/

LIBS_DIR        = libs/

CFLAGS          = -Wall -Wextra -Werror -I ${INCLS_DIR}

CC				= gcc

SRCS			= pipex.c\
					first_command.c\
					second_command.c\
					ft_split.c\
					ft_strjoin.c\
					utils.c\

OBJS            = ${SRCS:.c=.o}

OBJS            := ${addprefix ${OBJS_DIR}, ${OBJS}}

${OBJS_DIR}%.o: ${SRCS_DIR}%.c
				${CC} ${CFLAGS} -c $< -o $@

all:            ${NAME}

${NAME}:		${OBJS}
				${CC} -o $@ ${OBJS}

sanitize:		${OBJS}
				${CC} -o sanitize ${OBJS} -g3 -fsanitize=address
clean:
				${RM} ${OBJS}

fclean:			clean
				${RM} ${NAME}

re:				fclean all

RM				= rm -f
