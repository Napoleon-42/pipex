/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lnelson <lnelson@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/19 17:49:33 by lnelson           #+#    #+#             */
/*   Updated: 2021/10/19 17:49:49 by lnelson          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"

static int	ft_count_word(char const *s1, char c)
{
	int	comp;
	int	cles;

	comp = 0;
	cles = 0;
	if (*s1 == '\0')
		return (0);
	while (*s1 != '\0')
	{
		if (*s1 == c)
			cles = 0;
		else if (cles == 0)
		{
			cles = 1;
			comp++;
		}
		s1++;
	}
	return (comp);
}

static int	ft_size_w(char const *str, int i, char c)
{
	int	j;

	j = 0;
	while (str[i] != c && str[i] != '\0')
	{
		i++;
		j++;
	}
	return (j);
}

char	**ft_split(char const *s, char c)
{
	char	**res;
	int		i;
	int		j;
	int		n_word;

	if (!s || !c)
		return (0);
	i = 0;
	n_word = 0;
	res = malloc(sizeof(char *) * (ft_count_word((char *)s, c) + 1));
	while (n_word != ft_count_word((char *)s, c))
	{
		while (s[i] && s[i] == c)
			i++;
		res[n_word] = malloc(sizeof(char) * (1 + ft_size_w(s, i, c)));
		j = 0;
		while (s[i] != c && s[i])
			res[n_word][j++] = s[i++];
		res[n_word][j] = 0;
		n_word++;
	}
	res[n_word] = 0;
	return (res);
}
