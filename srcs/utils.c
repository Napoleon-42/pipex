/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lnelson <lnelson@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/19 14:06:47 by lnelson           #+#    #+#             */
/*   Updated: 2021/10/19 14:07:00 by lnelson          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"

int	error_exit(char *str)
{
	int	i;

	i = 0;
	while (str[i])
	{
		write(2, &(str[i]), 1);
		i++;
	}
	return (-1);
}

char	**get_paths(char **env)
{
	int		i;
	char	**res;
	char	*temp;

	i = 0;
	while (env[i][0] != 'P' || env[i][1] != 'A' || env[i][2] != 'T'
			|| env[i][3] != 'H' || env[i][4] != '=')
		i++;
	res = ft_split((char const *) env[i], ':');
	i = 0;
	while (res[i])
	{
		temp = res[i];
		res[i] = ft_strjoin(res[i], "/");
		if (temp)
			free(temp);
		i++;
	}
	return (res);
}

void	free_path(char **path)
{
	int	i;

	i = 0;
	while (path[i])
	{
		free(path[i]);
		i++;
	}
	free(path);
	return ;
}

void	add_path_utils(char **cmd, char **env, int i)
{
	char	*temp;

	temp = *cmd;
	*cmd = ft_strjoin(env[i], *cmd);
	if (temp)
		free(temp);
	return ;
}

int	add_path(char **cmd, char **env)
{
	char	*temp;
	char	**paths;
	int		i;

	i = 0;
	paths = get_paths(env);
	temp = ft_strjoin(paths[i], *cmd);
	while (access(temp, X_OK) != 0 && paths[i])
	{
		if (temp)
			free(temp);
		temp = ft_strjoin(paths[i], *cmd);
		i++;
	}
	if (access(temp, X_OK) == 0)
		add_path_utils(cmd, paths, i - 1);
	if (temp)
	{
		free(temp);
	}
	free_path(paths);
	return (1);
}
