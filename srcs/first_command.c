/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   first_command.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lnelson <lnelson@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/19 14:06:45 by lnelson           #+#    #+#             */
/*   Updated: 2021/10/30 18:29:13 by lnelson          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"

int	parent_crash(char **str_arr)
{
	int	i;

	i = 0;
	while (str_arr[i])
	{
		free(str_arr[i]);
		i++;
	}
	free(str_arr);
	write(2, "PIPEX: second command not found\n", 32);
	return (-1);
}

int	first_command(int pipefd[2], char **av, char **env)
{
	int		t;
	int		fd;
	char	**argv;

	fd = open(av[1], O_RDONLY);
	if (fd == -1)
		return (error_exit("could not open/create the <out_file>\n"));
	argv = ft_split(av[2], ' ');
	if (add_path(&(argv[0]), env) == -1)
		return (error_exit("the second command not found\n"));
	close(pipefd[0]);
	dup2(pipefd[1], 1);
	dup2(fd, 0);
	t = execve(argv[0], argv, NULL);
	if (t == -1)
		return (parent_crash(argv));
	close(pipefd[1]);
	close(fd);
	return (0);
}
