/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   second_command.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lnelson <lnelson@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/19 14:06:43 by lnelson           #+#    #+#             */
/*   Updated: 2021/10/30 18:24:28 by lnelson          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"

int	child_crash(char **str_arr)
{
	int	i;

	i = 0;
	while (str_arr[i])
	{
		free(str_arr[i]);
		i++;
	}
	free(str_arr);
	write(2, "PIPEX: first command not found\n", 31);
	return (-1);
}

int	second_command(int pipefd[2], char **av, char **env)
{
	int		t;
	int		fd;
	char	**argv;

	fd = open(av[4], O_WRONLY | O_CREAT | O_TRUNC, 0666);
	if (fd == -1)
		return (error_exit("in_file not found\n"));
	argv = ft_split(av[3], ' ');
	if (add_path(&(argv[0]), env) == -1)
		return (error_exit("the first command not found\n"));
	close(pipefd[1]);
	dup2(pipefd[0], 0);
	dup2(fd, 1);
	t = execve(argv[0], argv, NULL);
	if (t == -1)
		return (child_crash(argv));
	close(pipefd[0]);
	close(fd);
	return (0);
}
