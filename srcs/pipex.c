/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipex.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lnelson <lnelson@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/19 14:06:38 by lnelson           #+#    #+#             */
/*   Updated: 2021/10/30 18:28:37 by lnelson          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"

int	get_str_count(char **strs)
{
	int	res;

	res = 0;
	while (strs[res])
		res++;
	return (res);
}

int	main(int ac, char **av, char **env)
{
	int	pipefd[2];
	int	pid[2];

	if (ac != 5)
		return (error_exit("PIPEX: Wrong argument number\n"));
	pipe(pipefd);
	pid[0] = fork();
	if (pid[0] == -1)
		return (error_exit("PIPEX: first fork() faillure\n"));
	if (pid[0] == 0)
		first_command(pipefd, av, env);
	else
	{
		pid[1] = fork();
		if (pid[1] == -1)
			return (error_exit("PIPEX: second fork() faillure\n"));
		if (pid[1] == 0)
			second_command(pipefd, av, env);
	}
	close(pipefd[0]);
	close(pipefd[1]);
	waitpid(pid[0], &(pipefd[0]), 0);
	waitpid(pid[1], &(pipefd[1]), 0);
	return (0);
}
