/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipex.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lnelson <lnelson@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/19 14:06:41 by lnelson           #+#    #+#             */
/*   Updated: 2021/10/30 18:24:31 by lnelson          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PIPEX_H
# define PIPEX_H
# include <unistd.h>
# include <stdlib.h>
# include <fcntl.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <sys/wait.h>

char	**ft_split(char const *s, char c);
char	*ft_strjoin(char const *s1, char const *s2);
int		child_crash(char **str_arr);
int		parent_crash(char **str_arr);
int		error_exit(char *str);
int		first_command(int pipefd[2], char **av, char **env);
int		second_command(int pipefd[2], char **av, char **env);
char	**get_paths(char **env);
void	free_path(char **path);
void	add_path_utils(char **cmd, char **env, int i);
int		add_path(char **cmd, char **env);
int		error_exit(char *str);

#endif
